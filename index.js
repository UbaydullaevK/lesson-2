// question 1
function result(N){
    for(let num = 0; num < N; num++){
        if (num % 3 == 0 && num % 5 == 0)
        console.log(num+ " ");
    }
}
let N = 150;
result(N);

// question 2

function isEven(n) { return (n % 2 == 0); }
 
    let n = 101;
    isEven(n) ? console.log("Even") :console.log("Odd");

// question 3
let numbers = [4 , 3 , 5 , 1, 2];
numbers.sort(function(a, b) {
  return a - b;
});

console.log(numbers);
